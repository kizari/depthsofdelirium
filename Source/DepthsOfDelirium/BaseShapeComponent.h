// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseShapeComponent.generated.h"

UCLASS()
class UBaseShapeComponent : public UObject
{
	GENERATED_BODY()

public:

	UPROPERTY()
	UBaseShapeComponent *NorthTile = nullptr;

	UPROPERTY()
	UBaseShapeComponent *EastTile = nullptr;

	UPROPERTY()
	UBaseShapeComponent *SouthTile = nullptr;

	UPROPERTY()
	UBaseShapeComponent *WestTile = nullptr;

	UPROPERTY()
	bool bCanConnectNorth = false;

	UPROPERTY()
	bool bCanConnectEast = false;

	UPROPERTY()
	bool bCanConnectSouth = false;

	UPROPERTY()
	bool bCanConnectWest = false;
};